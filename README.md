CONTENTS OF THIS FILE
=====================
  * Introduction
  * Requirements
  * Installation
  * Configuration


INTRODUCTION
============
  * The Entity Deep Clone module allows fields and 
  subfields, including entity inline form fields to
  be carried over on a clone action.

REQUIREMENTS
============
Requires the following modules:
 * entity  
 * inline_entity_form


INSTALLATION
============
 * Installation for this module should be as simple as installing the module in
   the UI.


CONFIGURATION
============
  * This module contains no configuration or permissions
